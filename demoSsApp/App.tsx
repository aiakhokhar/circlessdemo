import React, { useState, useRef, useEffect, useCallback } from 'react';
import { View, StyleSheet, Alert ,} from 'react-native';
// import {  State } from 'react-native-gesture-handler';
import { GestureEvent, PanGestureHandler, PanGestureHandlerEventPayload } from 'react-native-gesture-handler';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

import Svg, { Circle } from 'react-native-svg';
import ViewShot from "react-native-view-shot";

const CircleDrawer = () => {
  const ref = useRef();
  const [circleCoordinates, setCircleCoordinates] = useState({ x: 0, y: 0 });
  const [start, setStart] = useState<{ x: number; y: number }>(null);
  const [end, setEnd] = useState<{ x: number; y: number }>({ x: 0, y: 0 });
  const [dimensions, setDimensions] = useState<{ w: number; h: number }>({ w: 0, h: 0 });

  const onPress = (event: GestureEvent<PanGestureHandlerEventPayload>) => {
    const { x, y, translationX, translationY } = event.nativeEvent;
    if (!start) setStart({ x: y, y: x });
    setDimensions({ w: translationX, h: translationY });
  };

  const onEnd = () => {
    if (!start) {
      onCapture()
      return
    };

    setEnd(start);
    setStart(null);
    
  };

  // const onCircleDraw = ({ nativeEvent }) => {
  //   console.log(nativeEvent)
  //   if (nativeEvent.state === State.END) {
  //     console.log( nativeEvent.x)
  //     setCircleCoordinates({
  //       x: nativeEvent.x,
  //       y: nativeEvent.y,
  //     });
  //   }
  // };
  const onCircleDraw = (nativeEvent) => {
    console.log(nativeEvent)
    return true
  };
  
  
  // const ref = useRef();

  const onCapture = useCallback(uri => {
    console.log("do something with ", uri);
  }, []);
  useEffect(() => {
    // onCapture()
    //  takeScreenshot()

  }, [])
  const takeScreenshot = () => {
    ref.current.capture().then(uri => {
      console.log("do something with ", uri);
    });
  }
//  const panResponder = useRef(
//     PanResponder.create({
//       onMoveShouldSetPanResponder: (evt, gestureState) => {
//         console.log(gestureState.dx)
//         return Math.abs(gestureState.dx) > 5;
//       },
//       // onStartShouldSetPanResponderCapture: () => {
//       //   resetInactivityTimeout();
//       // },
//     }),
//   ).current;
  return (
    <GestureHandlerRootView style={styles.container}>
     
      <ViewShot ref={ref} onCapture={onCapture}
       captureMode="update"
       options={{ fileName: "demoimg", format: "jpg", quality: 0.9 }}
       >
     
        {/* <PanGestureHandler onHandlerStateChange={onCircleDraw}> */}
        {/* <View 
        //  {...panResponder.panHandlers}
          style={styles.drawingContainer}>
          <Svg>
            <Circle
              cx={circleCoordinates.x}
              cy={circleCoordinates.y}
              r={30}
              stroke="black"
              strokeWidth={3}
              fill="none"
            />
          </Svg>
        </View> */}
      {/* </PanGestureHandler> */}
      <PanGestureHandler onGestureEvent={onPress} onEnded={onEnd}>
        <View style={{ width: 400, height: 400, backgroundColor: 'white' }}>
          <View
            style={{
              position: 'absolute',
              backgroundColor: 'blue',
              top: start?.x ?? end?.x,
              left: start?.y ?? end?.y,
              width: dimensions?.w ?? 0,
              height: dimensions?.h ?? 0,
              borderRadius:dimensions?.h/2 ?? 0
            }}
          />
        </View>
      </PanGestureHandler>
       
      </ViewShot>
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "yellow"
  },
  drawingContainer: {
    width: 300,
    height: 300,
    backgroundColor: 'grey',

  },
});

export default CircleDrawer;
